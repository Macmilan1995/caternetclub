﻿using CaternetClub.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaternetClub.Infrastracture
{
    public interface ISeatBookingSystem
    {
        Task<bool> BookSeat(List<SeatsToBookDto> seatsToBook, int userId);
    }
}
