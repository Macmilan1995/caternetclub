﻿using CaternetClub.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaternetClub.Infrastracture
{
    public class PaymentManager : IPaymentManager, ICryptoCurrencyManager
    {
        private readonly StoreContext _context;

        public PaymentManager(StoreContext context)
        {
            _context = context;
        }

        public Task<bool> IsAuthorized()
        {
            throw new NotImplementedException();
        }

        public Task MakeBankTransfer()
        {
            throw new NotImplementedException();
        }

        public Task MakeCardPayment()
        {
            throw new NotImplementedException();
        }

        public Task MakePayment()
        {
            throw new NotImplementedException();
        }

        public Task PayInCryptoCurrency()
        {
            throw new NotImplementedException();
        }
    }
}
