﻿using CaternetClub.DAL;
using CaternetClub.Dtos;
using CaternetClub.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaternetClub.Infrastracture
{
    public class SeatBookingSystem : ISeatBookingSystem
    {
        private readonly StoreContext _context;

        public SeatBookingSystem(StoreContext context)
        {
            _context = context;
        }

        public async Task<bool> BookSeat(List<SeatsToBookDto> seatsToBook, int userId)
        {
            if (SeatsAvailable(seatsToBook,userId))
            {

                foreach (var seat in seatsToBook)
                {
                    var availableSeat = await _context.Seats.FirstAsync(x => x.Label == seat.Label);

                    Orders order = new Orders()
                    {
                        UserId = userId,
                        SeatId = availableSeat.SeatId,
                        Email=seat.Email,
                        Name=seat.Name
                    };
                    try
                    {
                        await _context.Orders.AddAsync(order);
                        await _context.SaveChangesAsync();
                        UpdateStatus(availableSeat.SeatId);
                    }
                    catch (Exception message)
                    {

                        return false ;
                    }
                

                }
                return true;
            }
            return false;
        }
        private void UpdateStatus(int SeatId)
        {
            var seat = _context.Seats.First(x => x.SeatId == SeatId);
            seat.StatusID = 2;
            _context.Seats.Update(seat);
        }
        private bool SeatsAvailable(List<SeatsToBookDto> seatsToBook,int userId)
        {
            //check ifseatsExists potentially new method if needed.If seat doesnt exist in database frontend can cause the issue. 
            if (!IsAllowedToBook(seatsToBook, userId))
                return false;
          
            foreach (var seat in seatsToBook)
            {
                bool seatExists = _context.Seats.Any(x => x.Label == seat.Label);
                if (!seatExists)
                {
                    return false;
                }
            }

            //check whether seats has been booked
            foreach (var seat in seatsToBook)
            {

                bool bookedSeats = _context.Orders.Any(x => x.Seats.Label == seat.Label);
                if (bookedSeats)
                {
                    return false;
                }
            }
            return true;

        }
        private bool IsAllowedToBook(List<SeatsToBookDto> seatsToBook, int userId)
        {
            var numberofSeatsBooked = _context.Orders.Where(x => x.UserId == userId).Count();
            var numberofSeatsAfterBooking = numberofSeatsBooked + seatsToBook.Count();

            if (numberofSeatsAfterBooking > 4)
            {
                return false;
            }
            return true;
        }

    }
}
