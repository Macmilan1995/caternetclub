﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaternetClub.Infrastracture
{
   public interface IPaymentManager
    {
        Task<bool> IsAuthorized();

        Task MakePayment();

        Task MakeCardPayment();

        Task MakeBankTransfer();

    }

    public interface ICryptoCurrencyManager
    {
        Task PayInCryptoCurrency();
    }
}
