﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CaternetClub.Migrations
{
    public partial class updateOrdertable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Seats",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Seats",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "Seats");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Seats");
        }
    }
}
