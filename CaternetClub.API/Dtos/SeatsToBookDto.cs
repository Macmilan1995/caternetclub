﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaternetClub.Dtos
{
    public class SeatsToBookDto
    {
        public string Label { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }
    }
}
