﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CaternetClub.Model
{
    public class Seats
    {
        [Key]
        public int SeatId { get; set; }

        public string Label { get; set; }


        public int StatusID { get; set; }
        
        public virtual SeatStatus Status { get; set; }

    }
}
