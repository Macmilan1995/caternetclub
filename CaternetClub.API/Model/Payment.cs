﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaternetClub.Model
{
    public class Payment
    {
        public int PaymentId { get; set; }

        public int OrderId { get; set; }

        public int UserId { get; set; }

        public int PaymentStatusId { get; set; }

        public virtual PaymentStatus PaymentStatus {get;set;}

        public virtual Users Users { get; set; }

        public virtual Orders Orders { get; set; }

    }
}
