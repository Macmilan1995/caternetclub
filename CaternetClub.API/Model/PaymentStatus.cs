﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaternetClub.Model
{
    public class PaymentStatus
    {
        public int PaymentStatusID { get; set; }

        public string Name { get; set; }
    }
}
