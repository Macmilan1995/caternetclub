﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CaternetClub.Model
{
    public class Orders
    {
        [Key]
        public int OrderId { get; set; }
        
        public int SeatId { get; set; }

        [Required]
        public string Name { get; set; }

        [EmailAddress]
        [Required]
        public string Email { get; set; }

        public int UserId { get; set; }
        
        public virtual Users Users { get; set; }
        
        public virtual Seats Seats { get; set; }
    }
}
