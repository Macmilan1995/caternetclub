﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CaternetClub.Infrastracture;
using Microsoft.AspNetCore.Mvc;

namespace CaternetClub.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly IPaymentManager _paymentManager;
        private readonly ICryptoCurrencyManager _cryptoCurrencyManager;
        public PaymentController(IPaymentManager paymentManager, ICryptoCurrencyManager cryptoCurrencyManager)
        {
            _paymentManager = paymentManager;
            _cryptoCurrencyManager = cryptoCurrencyManager;
        }
        [HttpPost]
        public async Task<IActionResult> MakeBankTransfer()
        {
           await _paymentManager.MakeBankTransfer();
            return StatusCode(501);
        }
        [HttpPost]
        public async Task<IActionResult> PayinBitcoin()
        {
            await _cryptoCurrencyManager.PayInCryptoCurrency();
            return StatusCode(501);
        }
    }
}