﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CaternetClub.Dtos;
using CaternetClub.Infrastracture;
using Microsoft.AspNetCore.Mvc;

namespace CaternetClub.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservationController : ControllerBase
    {
        private readonly ISeatBookingSystem _seatBookingSystem;
        public ReservationController(ISeatBookingSystem seatBookingSystem)
        {
            _seatBookingSystem = seatBookingSystem;
        }
        [HttpPost]
        public async Task<IActionResult> BookSeat(List<SeatsToBookDto> seatsToBookDtos,int UserId)
        {
           bool result =  await _seatBookingSystem.BookSeat(seatsToBookDtos, UserId);
            if (result)
            {
                return StatusCode(200);
            }
            else
            {
                return StatusCode(500,new { message="There were problems with your call.Please contact our developer team,we have not implemented any errorLoggin Interface ;/" });
            }
           
        } 

    }
}