﻿using CaternetClub.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaternetClub.DAL
{
    public class StoreContext : DbContext
    {
      
        public StoreContext(DbContextOptions<StoreContext> options) : base(options)
        {
           

        }

        public DbSet<Users> Users { get; set; }

        public virtual DbSet<Seats> Seats { get; set; }

        public DbSet<SeatStatus> SeatStatus { get; set; }

        public DbSet<Orders> Orders { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }
    }
}
