﻿using CaternetClub.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaternetClub.DAL
{
    public class StoreInitializer : IStoreInitializer
    {
        private readonly StoreContext _context;
        public StoreInitializer(StoreContext context)
        {
            _context = context;
       
        }
        public void SeedSeats()
        {

            SeedStatus();
            SeedUser();
            var Letters = getSeatsLetters();
            for (var i = 1; i <= 10; i++)
            {
                for (var j = Letters.First().Key; j <= Letters.Count(); j++)
                {
                  
                    string letter = Letters.First(x => x.Key == j).Value;
                    _context.Seats.Add(new Model.Seats
                    {
                        Label = $"{letter}{i}",
                        StatusID =1
                    });
                    _context.SaveChanges();
                }
            }
        }
        public IDictionary<int, string> getSeatsLetters()
        {
           
           IDictionary<int, string> Letters = new Dictionary<int, string>()
            {
              { 1,"A"},
              { 2,"B"},
              { 3,"C"},
              { 4,"D"},
              { 5,"E"},
              { 6,"F"},
              { 7,"G"},
              { 8,"H"},
              { 9,"I"},
              { 10,"J"},
            };

            return Letters;
        }
        private void SeedUser()
        {
            Users user = new Users()
            {
                Email = "default@gmail.com",
                Name = "Default"
            };
            Users user1 = new Users()
            {
                Email = "Patryk@gmail.com",
                Name = "Patryk"
            };
            _context.Users.Add(user);
            _context.Users.Add(user1);
            _context.SaveChanges();
        }
        private void SeedStatus()
        {
            SeatStatus seatStatus = new SeatStatus()
            {
                Id = 1,
                Name = "Available"
            };
            SeatStatus seatStatus1 = new SeatStatus()
            {
                Id = 2,
                Name = "Unavailable"
            };
            _context.SeatStatus.Add(seatStatus);
            _context.SeatStatus.Add(seatStatus1);
            _context.SaveChanges();
        }
    }
}
