﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaternetClub.DAL
{
    public interface IStoreInitializer
    {
        void SeedSeats();

        IDictionary<int, string> getSeatsLetters();
    }
}
