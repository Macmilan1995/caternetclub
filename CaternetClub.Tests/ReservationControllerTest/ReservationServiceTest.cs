﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using CaternetClub;
using CaternetClub.Controllers;
using Moq;
using CaternetClub.DAL;
using CaternetClub.Infrastracture;
using CaternetClub.Dtos;
using System.Threading.Tasks;
using CaternetClub.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace CaternetClub.Tests.ReservationControllerTest
{
    public class ReservationServiceTest
    {



        [Fact]
        public async Task ReserveSeat()
        {
            //arrange
            populateData("ReserveSeat");
            List<SeatsToBookDto> seats = new List<SeatsToBookDto>()
            {
                new SeatsToBookDto
                {
                  Label= "A1",
                  Name = "Jack",
                  Email="jackbrown@gmail.com"
                },
                new SeatsToBookDto
                {
                    Label="A3",
                    Name = "John",
                    Email="johnwhite@gmail.com"
                },
                new SeatsToBookDto
                {
                    Label="A3",
                    Name = "Luke",
                    Email="Luke@gmail.com"
                }


            };
            var options = new DbContextOptionsBuilder<StoreContext>()
                 .UseInMemoryDatabase(databaseName: "ReserveSeat")
                 .Options;

            bool result;

            using (var context = new StoreContext(options))
            {
                var service = new SeatBookingSystem(context);
                //act
                result = await service.BookSeat(seats, 1);
            }
            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = new StoreContext(options))
            {
                //assert
                Assert.True(result == true);
            }



        }
        [Fact]
        public async Task Reserve4Seats()
        {
            populateData("Reserve4Seats");
            List<SeatsToBookDto> seats = new List<SeatsToBookDto>()
            {
                new SeatsToBookDto
                {
                  Label= "A1",
                  Name = "Kate",
                  Email="Kate@gmail.com"
                },
                new SeatsToBookDto
                {
                    Label="A3",
                    Name = "Henry",
                    Email="Henry@gmail.com"
                },
                  new SeatsToBookDto
                {
                    Label="B3",
                    Name = "Jerry",
                    Email="Jerry@gmail.com"
                },
                    new SeatsToBookDto
                {
                    Label="C3",
                    Name = "William",
                    Email="William@gmail.com"
                }


            };
            var options = new DbContextOptionsBuilder<StoreContext>()
                .UseInMemoryDatabase(databaseName: "Reserve4Seats")
                .Options;

            bool result;

            using (var context = new StoreContext(options))
            {
                var service = new SeatBookingSystem(context);
                //act
                result = await service.BookSeat(seats, 1);
            }
            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = new StoreContext(options))
            {
                //assert
                Assert.True(result == true);
            }




        }
        [Fact]
        public async Task ReserveTooManySeats()
        {
            populateData("ReserveTooManySeats");
            List<SeatsToBookDto> seats = new List<SeatsToBookDto>()
            {
                new SeatsToBookDto
                {
                  Label= "A1",
                   Name = "John1",
                    Email="johnwhite1@gmail.com"
                },
                new SeatsToBookDto
                {
                    Label="A3",
                     Name = "John2",
                    Email="johnwhite2@gmail.com"
                },
                  new SeatsToBookDto
                {
                    Label="B3",
                     Name = "John3",
                    Email="johnwhite3@gmail.com"
                },
                    new SeatsToBookDto
                {
                    Label="C3",
                     Name = "John4",
                    Email="johnwhite4@gmail.com"
                }


            };
            List<SeatsToBookDto> seats2 = new List<SeatsToBookDto>()
            {
                new SeatsToBookDto
                {
                  Label= "B2",
                  Name = "John5",
                  Email="johnwhite5@gmail.com"
                }

            };
            var options = new DbContextOptionsBuilder<StoreContext>()
                .UseInMemoryDatabase(databaseName: "ReserveTooManySeats")
                .Options;

            bool result;
            bool result2;
            using (var context = new StoreContext(options))
            {
                var service = new SeatBookingSystem(context);
                //act
                //check 4 seats, should be fine
                result = await service.BookSeat(seats, 1);
                //add one more and we should get false
                result2 = await service.BookSeat(seats2, 1);
            }
            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = new StoreContext(options))
            {
                //assert
                Assert.True(result == true && result2 == false);
            }




        }

        private void populateData(string databaseName)
        {
            var options = new DbContextOptionsBuilder<StoreContext>()
              .UseInMemoryDatabase(databaseName: databaseName)
              .Options;
            var context = new StoreContext(options);
            var storeInitializer = new StoreInitializer(context);
            storeInitializer.SeedSeats();


        }
    }
}
